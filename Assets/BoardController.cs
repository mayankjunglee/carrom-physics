using Lean.Touch;
using System;
using System.Collections.Generic;
using UnityEngine;

public class BoardController : MonoBehaviour
{
    public Pucks[] pucksList;
    public LineRenderer line;
    
    private Circle activeCircle;

    private Vector2 offset;
    [SerializeField][Range(1,4)] int simulationUpdate = 4;
    float fDeltaTime = 0f;
    [SerializeField] [Range(1, 15)] int nMaxSimulation = 15;
    [SerializeField] bool simulate;
    [SerializeField] float wallDelta = 4.75f;
    private void Awake()
    {
        fDeltaTime = Time.fixedDeltaTime / simulationUpdate;
    }

    private void OnEnable()
    {
        LeanTouch.OnFingerDown += LeanTouch_OnFingerDown;
        LeanTouch.OnFingerUpdate += LeanTouch_OnFingerUpdate;
        LeanTouch.OnFingerUp += LeanTouch_OnFingerUp;
    }

    private void LeanTouch_OnFingerUp(LeanFinger obj)
    {
        if (activeCircle != null) 
        { 
            Vector2 touchPos = obj.GetWorldPosition(0);

            //Vector3 updatePos = touchPos - offset;
            //updatePos.z = activeCircle.transform.position.z;
            //activeCircle.transform.position = updatePos;

            float force = Vector2.Distance(touchPos, activeCircle.position);
            Vector2 dir = (touchPos - (Vector2)activeCircle.position).normalized;
            activeCircle.velocity = -5.0f*dir * force;

            activeCircle = null;
            simulate = true;
            DeactivateLine();
        }
    }

    private void LeanTouch_OnFingerUpdate(LeanFinger obj)
    {
        if (activeCircle != null)
        {
            Vector2 touchPos = obj.GetWorldPosition(0);
            //Vector3 updatePos = touchPos - offset;
            //updatePos.z = activeCircle.transform.position.z;
            //activeCircle.transform.position = updatePos;
            DrawLine(activeCircle.position, touchPos);
        }
    }

    private void LeanTouch_OnFingerDown(LeanFinger obj)
    {
        Vector2 touchPos = obj.GetWorldPosition(0);

        activeCircle = GetCircleInWorldPosition(touchPos);
        if (activeCircle != null) {
            offset = (touchPos - activeCircle.position);
            DrawLine(activeCircle.position, touchPos);
        }
    }

    private Circle GetCircleInWorldPosition(Vector2 touchPos)
    {
        Circle circle = null;
        for (int i = 0; i < pucksList.Length; ++i)
        {
            Circle temp = pucksList[i].circle;
            Vector2 circlePos = temp.position;
            float distanceSquare = DistanceSquareBetweenXYPlane(touchPos, circlePos);
            if (distanceSquare < temp.radiusSquare)
            {
                circle = temp;
                break;
            }
        }
        return circle;
    }

    void DrawLine(Vector2 pos1,Vector2 pos2)
    {
        if (line == null) return;
        line.gameObject.SetActive(true);
        line.SetPosition(0,pos1);
        line.SetPosition(1,pos2);
    }

    void DeactivateLine()
    {
        if (line == null) return;
        line.gameObject.SetActive(false);
    }

    private static float DistanceSquareBetweenXYPlane(Vector2 pos1, Vector2 pos2)
    {
        float dx = pos1.x - pos2.x;
        float dy = pos1.y - pos2.y;
        float distanceSquare = dx * dx + dy * dy;
        return distanceSquare;
    }

    private void OnDisable()
    {
        LeanTouch.OnFingerDown -= LeanTouch_OnFingerDown;
        LeanTouch.OnFingerUpdate -= LeanTouch_OnFingerUpdate;
        LeanTouch.OnFingerUp -= LeanTouch_OnFingerUp;
    }

    private void FixedUpdate()
    {
        //if (!simulate) return;
        for (int i = 0; i < simulationUpdate; ++i)
        {
            for (int j = 0; j < pucksList.Length; ++j)
            {
                pucksList[j].circle.deltaTimeRemaining = fDeltaTime;
            }

            for (int j = 0; j < nMaxSimulation; ++j)
            { 
                UpdateVelocities();
                List<OverlappedCircle> overlappedCirclesList =  HandleOverlappedCircles();
                HandleDynamicPucksCollision(overlappedCirclesList);
            }
        }
        SetVelocities();
        simulate = false;
    }

    private void SetVelocities()
    {
        for (int i = 0; i < pucksList.Length; ++i)
        {
            pucksList[i].transform.position = pucksList[i].circle.position;
        }
    }


    private void HandleDynamicPucksCollision(List<OverlappedCircle> overlappedCirclesList)
    {
        for (int i = 0; i < overlappedCirclesList.Count; ++i)
        {
            Circle one = overlappedCirclesList[i].circle1;
            Circle two = overlappedCirclesList[i].circle2;

            float dist = Vector2.Distance(one.position,two.position);

            Vector2 normal = (Vector2)(two.position - one.position) / dist;
            Vector2 tangent = new Vector2(-normal.y,normal.x);

            float dptan1 = one.velocity.x * tangent.x + one.velocity.y * tangent.y;
            float dptan2 = two.velocity.x * tangent.x + two.velocity.y * tangent.y;
            
            float dpNorm1 = one.velocity.x * normal.x + one.velocity.y * normal.y;
            float dpNorm2 = two.velocity.x * normal.x + two.velocity.y * normal.y;

            float m1 = (dpNorm1*(one.mass -two.mass)+2.0f*two.mass *dpNorm2)/(one.mass +two.mass);
            float m2 = (dpNorm2*(two.mass -one.mass)+2.0f*one.mass *dpNorm1)/(one.mass +two.mass);

            one.velocity = tangent * dptan1+normal*m1;
            two.velocity = tangent * dptan2+normal*m2;


        }
    }

    private void UpdateVelocities()
    {
        for (int i = 0; i < pucksList.Length; ++i) 
        {
            Circle circle = pucksList[i].circle;
            if (circle.deltaTimeRemaining > 0.0f)
            {
                circle.oldPosition = circle.position;
                circle.acceleration = -circle.velocity * .8f;
                circle.velocity += circle.acceleration * circle.deltaTimeRemaining;
                Vector2 updatedPos = circle.position + circle.velocity * circle.deltaTimeRemaining;
                circle.position = updatedPos;
                if (Vector2.SqrMagnitude(circle.velocity) < 0.01f) 
                {
                    circle.velocity = Vector2.zero;
                }
            }
        }
    }

    List<OverlappedCircle> HandleOverlappedCircles()
    {
        List<OverlappedCircle> overlappedCirclesList = new List<OverlappedCircle>();
        for (int i = 0; i < pucksList.Length; ++i) 
        {
            for (int wall = 0; wall < 4; ++wall)
            {
                float delta = wallDelta + pucksList[i].circle.radius;
                Vector2 fakeCirPos = GetClosestFakeCirclePosFromWallNo(wall, pucksList[i].circle,delta);
                float dist = GetDistanceFromWall(pucksList[i].circle,wall,delta);
                if (dist < 2*pucksList[i].circle.radius)
                {
                    Circle fakeCircle = new Circle {
                        position = fakeCirPos,
                        radius = pucksList[i].circle.radius,
                        mass = pucksList[i].circle.mass,
                        velocity = -pucksList[i].circle.velocity
                    };
                    
                    float overlap = dist - 2 * pucksList[i].circle.radius;

                    pucksList[i].circle.position -= overlap * (pucksList[i].circle.position - fakeCirPos) / dist;
                    overlappedCirclesList.Add(new OverlappedCircle(pucksList[i].circle,fakeCircle));
                }
            }

            for (int j = 0; j < pucksList.Length; ++j)
            {
                if (i == j) continue;
                Circle one = pucksList[i].circle;
                Circle two = pucksList[j].circle;
                float distanceSquareBetweenTwoCicles = DistanceSquareBetweenXYPlane(one.position,two.position);
                if (distanceSquareBetweenTwoCicles < 4*one.radiusSquare)
                {
                    float distanceBetweenTwoCircles = Mathf.Sqrt(distanceSquareBetweenTwoCicles);
                    float overlap = .5f * (distanceBetweenTwoCircles - 2 * one.radius);

                    Vector3 posOne = one.position;
                    Vector3 posTwo = two.position;
                    Vector2 dir = new Vector2(posOne.x - posTwo.x, posOne.y- posTwo.y) / distanceBetweenTwoCircles;
                    posOne.x -= overlap * dir.x;
                    posOne.y -= overlap * dir.y;
                    
                    posTwo.x += overlap * dir.x;
                    posTwo.y += overlap * dir.y;

                    one.position = posOne;
                    two.position = posTwo;
                    overlappedCirclesList.Add(new OverlappedCircle(one,two));
                }
            }

            float intendentSpeed = Vector2.SqrMagnitude(pucksList[i].circle.velocity);
            //float intendentDistance = intendentSpeed * pucksList[i].circle.deltaTimeRemaining;
            float actualDistance = Mathf.Sqrt(DistanceSquareBetweenXYPlane(pucksList[i].circle.position, pucksList[i].circle.oldPosition));
            float actualTime = actualDistance / intendentSpeed;

            pucksList[i].circle.deltaTimeRemaining -= actualTime;
        }
        return overlappedCirclesList;
    }

    private float GetDistanceFromWall(Circle circle, int wall,float delta)
    {
        switch (wall)
        {
            case 0:
                return Mathf.Abs(circle.position. x +delta);
            case 1:
                return Mathf.Abs(circle.position. y -delta);
            case 2:
                return Mathf.Abs(circle.position. x -delta);
            case 3:
                return Mathf.Abs(circle.position. y +delta);
            default:
                return 0;
        }
    }

    private Vector2 GetClosestFakeCirclePosFromWallNo(int i,Circle circle,float delta)
    { 
        switch (i) 
        {
            case 0:
                //left wall
                return new Vector2(-delta, circle.position.y);
            case 1:
                //top wall
                return new Vector2(circle.position.x, delta);
            case 2:
                //right wall
                return new Vector2(delta, circle.position.y);
            case 3:
                //bottom wall
                return new Vector2(circle.position.x, -delta);
    
        }
        return Vector2.zero;
    }

    public void OnSimulateButtonClick()
    {
        simulate = true;
    }
}

public class OverlappedCircle
{
    public Circle circle1;
    public Circle circle2;

    public OverlappedCircle(Circle c1, Circle c2)
    {
        circle1 = c1;
        circle2 = c2;
    }
}

public class OverlapBoundary 
{
    public Vector2 collidePoint;
    public Pucks circle;
    public WallSide wallSide;
    public OverlapBoundary(Pucks c,Vector2 point,WallSide wallSide)
    {
        this.circle = c;
        this.collidePoint = point;
        this.wallSide = wallSide;
    }
}

public enum WallSide
{
    Left,Top,Right,Bottom
}

[Serializable]
public class Circle
{
    public float radius;
    public float radiusSquare;
    public float mass = 1f;
    public Vector2 velocity = Vector2.zero;
    public Vector2 acceleration = Vector2.zero;
    public Vector2 position;
    public Vector2 oldPosition;
    public float deltaTimeRemaining;
}