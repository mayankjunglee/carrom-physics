using UnityEngine;

public class Pucks : MonoBehaviour
{
    Sprite sprite;
    public Circle circle;
    private void Awake()
    {
        sprite = GetComponent<SpriteRenderer>().sprite;
        circle = new Circle();
    }

    private void Start()
    {
        circle.radius = (sprite.bounds.max.x - sprite.bounds.min.x)/2;
        circle.radiusSquare = circle.radius * circle.radius;
        circle.position = transform.position;
    }
}
